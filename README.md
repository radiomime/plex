# Overview

This project's goal is to provide a consolidated page to set up plex using
docker and docker-compose. There are steps to mount external hard drives on
boot, if you're using some flavors of linux (raspbian for raspberry pi,
ubuntu, etc.).

### Dependencies

Install docker and docker-compose for your environment.

## Mount hard drive on boot [optional]

### Find the uuid for your disk

We need the hard drive to reliably mount in an expected place on every boot.
For this reason, we'll need the uuid of the hard drive. A few methods are
described below.

If you want to sanity check, use a couple methods. Also, plug in then unplug
your drive. The new entry is probably your drive.

#### blkid

This will just list a ton of block devices. You'll most likely see your hard
drive in there with a label.
```
sudo blkid | grep UUID=
```

Should produce something like
```
/dev/sdb2: LABEL="HD MANUFACTURER" UUID="ABCD1234" ...
```
We are intereseted in the UUID.

#### lsblk

Lists block devices in a pretty way. This is my favorite.

```
sudo lsblk  -f | grep -v loop
```

There should be a `LABEL` column with the drive manufacturer's name, and a
`UUID` column with the info we want.

#### ls

Your drive will probaby show up here by UUID, but this is the least descriptive
method.

```
ls /dev/disk/by-uuid/
```

### Hard drive to fstab

We must add our hard drive to our fstab file with our user information. This
way, on boot our drive will mount in an expected place, and our user will be
able to access this information.

#### Make mount directory

Naming is optional here, but `/media` is the common place to mount your drive.
`/mnt` works, too. I'm calling my folder `toshi` because it distinguished my
drive to me, but this should change.

```
sudo mkdir -p /media/toshi
```

### Set ownership of drive

Your user should own the mount point for the drive for docker things to work as
expected.

```
ls -alh /media
sudo chown -R $USER:$USER /media/toshi
ls -alh /media
```

Your user should own the directory now.

### Mount drive on boot via fstab entry

To find your uid and gid fields. `id -u $USER && id -g $USER`

The fstab file holds static filesystem info. We're editing it so our drive
mounts on boot.

```
sudo vim /etc/fstab
```

Then add the following line. Change things as needed, such as `UUID` and
`/media/toshi`. 

```
UUID=ABCD1234   /media/toshi    auto    nofail,uid=1001,gid=1001,noatime 0 0 
```

#### Check if things are mounting correctly with fstab

Unmount your drive by UUID, then mount your drive
```
umount /dev/disk/by-uuid/ABCD1234
sudo mount -a
ls -alh /media  # Should show your disk, mounted with your user as owner.
```

## Docker to run plex server

### Change environment variables

Copy `.sample.env` to `.env` and enter your information.

```
MOUNT_DIR=/media/toshi
OWNER_USER_ID=1005
OWNER_GROUP_ID=1005
```

### Run plex

```
docker-compose up -d
```

## Visit plex server

Visit your plex server at `<lan-addr-of-box>:32400/web`


# Resouces

- [Plex on docker](https://hub.docker.com/r/linuxserver/plex/)
- [Raspberry pi plex server](https://pimylifeup.com/raspberry-pi-plex-server/)
- [Mounting drive on boot](https://pimylifeup.com/raspberry-pi-mount-usb-drive/)
- [UUID of disk](https://linuxhint.com/uuid_storage_devices_linux/)

